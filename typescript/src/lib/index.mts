import {emitHtml as emit, parse} from "#wasm";

import {Root} from "./ast.mjs";
import {Grammar} from "./grammar.mjs";

export function parseMarktwin(grammar: Grammar, input: string): Root {
  return parse(grammar, input);
}

export function emitHtml(input: Root): string {
  return emit(input);
}

export function renderMarktwin(grammar: Grammar, input: string): string {
  return emit(parse(grammar, input));
}
