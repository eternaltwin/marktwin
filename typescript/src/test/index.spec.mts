import chai from "chai";
import fs from "fs";
import furi from "furi";
import url from "url";

import {NodeType, Root} from "../lib/ast.mjs";
import {Grammar} from "../lib/grammar.mjs";
import {parseMarktwin, renderMarktwin} from "../lib/index.mjs";

const PROJECT_ROOT: url.URL = furi.join(import.meta.url, "../../..");
const TEST_RESOURCES: url.URL = furi.join(PROJECT_ROOT, "test-resources");

const DEFAULT_GRAMMAR: Grammar = {
  admin: false,
  depth: null,
  emphasis: false,
  icons: [],
  links: [],
  mod: false,
  quote: false,
  spoiler: false,
  strong: false,
  strikethrough: false
};

describe("parse", () => {
  it("parses foo", () => {
    const id = parseMarktwin(DEFAULT_GRAMMAR, "foo");
    const expected: Root = {
      loc: null,
      children: [
        {
          type: NodeType.Text,
          loc: null,
          text: "foo",
        },
      ],
    };
    chai.assert.deepEqual(id, expected);
  });
});

describe("format", () => {
  for (const ent of fs.readdirSync(TEST_RESOURCES, {withFileTypes: true})) {
    if (!ent.isDirectory() || ent.name.startsWith(".")) {
      continue;
    }
    const sampleName: string = ent.name;
    const sampleDir: url.URL = furi.join(TEST_RESOURCES, [sampleName]);
    it(`format ${sampleName}`, () => {
      const mkt: string = fs.readFileSync(furi.join(sampleDir, "main.mkt"), {encoding: "utf-8"});
      const grammarJson: string = fs.readFileSync(furi.join(sampleDir, "grammar.json"), {encoding: "utf-8"});
      const grammar: Grammar = JSON.parse(grammarJson);
      const actual: string = renderMarktwin(grammar, mkt);
      fs.writeFileSync(furi.join(sampleDir, "local-main.html"), actual, {encoding: "utf-8"});
      const html: string = fs.readFileSync(furi.join(sampleDir, "main.html"), {encoding: "utf-8"});
      chai.assert.deepEqual(actual, html);
    });
  }
});
