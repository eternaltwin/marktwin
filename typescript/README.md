# Marktwin

Markup code used by Eternal-Twin for its rich text.

## Formatting

### Spans

```
**Strong (bold)**
_Emphasis (italic)_
~~Strike-through~~
[Link text](Link URI)
```

### Blocks

```
[quote][/quote]
[mod][/mod]
[admin][/admin]
[rp][/rp]
```code```
```

### Components

```
@{userId}
[poll][/poll]
```

### Icons

Icons are `[a-z_0-9]+` sequences between colons.

```
:slight_smile:
:tid:
:intrusion_ccard_50:
```

### Escape

You can escape the next formatting by prefixing it with a backslash.

## Restrictions

You can't nest blocks more than 5 times.

```
[mod][mod][mod][mod][mod]OK[/mod][/mod][/mod][/mod][/mod]
[mod][mod][mod][mod][mod][mod]Not OK[/mod][/mod][/mod][/mod][/mod][/mod]
```
