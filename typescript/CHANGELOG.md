# 0.5.0 (2023-12-03)

- **[Breaking change]** Bump Node version to 18.12.
- **[Fix]** Update dependencies.

# 0.4.3 (2021-10-28)

- **[Fix]** Fix subpath exports.

# 0.4.2 (2021-10-28)

- **[Fix]** Fix root export resolution.

# 0.4.1 (2021-10-26)

- **[Feature]** Use Rust implementation for `emitHtml`.

# 0.4.0 (2021-10-26)

- **[Breaking change]** Use context-aware HTML escaping.

# 0.3.0 (2021-10-18)

- **[Breaking change]** Complete migration to native ESM.

# 0.2.0 (2020-09-30)

- **[Breaking change]** Require grammar config for the parser
- **[Feature]** Add support for icons
- **[Feature]** Add support for links
- **[Feature]** Add support for maximum nesting depth limits
- **[Feature]** Add support for `admin` and `mod` blocks
- **[Feature]** Add support for escape sequences

# 0.1.1 (2020-06-18)

- **[Fix]** Fix missing dependency

# 0.1.0 (2020-06-18)

- **[Feature]** First release
