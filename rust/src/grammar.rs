use serde::{Deserialize, Serialize};
use std::collections::HashSet;

#[derive(Debug, Eq, PartialEq, Clone, Deserialize, Serialize)]
pub struct Grammar {
  /// Enable `[admin]foo[/admin]` blocks
  pub admin: bool,

  /// Maximum nesting depth.
  ///
  /// - `None` means there is no depth limit
  /// - `Some(0)` means that only `Text` and `NewLine` nodes are produced
  /// - `Some(1)` renders `_foo_` as `<em>foo</em>` but renders `**_foo_**` as `<strong>_foo_</strong>`.
  pub depth: Option<u8>,

  /// Enable `_foo_` spans
  pub emphasis: bool,

  /// Set of allowed icon keys
  pub icons: HashSet<String>,

  /// Set of allowed link protocols
  pub links: HashSet<String>,

  /// Enable `[mod]foo[/mod]` blocks
  pub r#mod: bool,

  /// Enable `[quote]foo[/quote]` blocks
  pub quote: bool,

  /// Enable `||foo||` spans
  pub spoiler: bool,

  /// Enable `**foo**` spans
  pub strong: bool,

  /// Enable `~~foo~~` spans
  pub strikethrough: bool,
}

impl Grammar {
  pub fn full() -> Self {
    Self {
      admin: true,
      emphasis: true,
      depth: Some(4),
      icons: {
        let mut icons = HashSet::new();
        icons.insert(String::from("etwin"));
        icons
      },
      links: {
        let mut links = HashSet::new();
        links.insert(String::from("http"));
        links.insert(String::from("https"));
        links
      },
      r#mod: true,
      quote: true,
      spoiler: true,
      strong: true,
      strikethrough: true,
    }
  }
}
