#![no_main]
use libfuzzer_sys::fuzz_target;

fuzz_target!(|data: String| {
   let cst = marktwin::parser::parse(&data);
   let _ = marktwin::parser::parse_owned_ast(&data);
});
