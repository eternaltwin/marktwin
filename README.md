# Marktwin main repository

## Getting started

### Requirements

- Rust stable
- Node
- Yarn

### Rust

```
cd rust
cargo test
```

### TypeScript

```
cd typescript
cargo install -f wasm-bindgen-cli
yarn install
yarn test
```
